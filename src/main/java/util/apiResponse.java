package util;

import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class apiResponse {
 
    // Method to check for 500 response
    public static void check500Error(WebDriver driver) throws AssertionError {
    	
	   try {
			// Set up explicit wait
			 WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(5));
   	        WebElement internalError = wait.until(ExpectedConditions.visibilityOfElementLocated(By.tagName("h1")));
	        String internalErrorMsg  = internalError.getText();
	        String expectedErrorMsg = "HTTPステータス 500 -";
	        String expectedErrorMsg1 = "HTTP Status 500 -";
	        driver.get("http://54.238.41.198:8080/exam/login");
	        if (internalErrorMsg.equals(expectedErrorMsg)) {
	            System.out.println("Internal server error message : " + internalErrorMsg);
	            // Explicit assertion for error condition
	            assert false : "Internal server error encountered";
	        } if (internalErrorMsg.equals(expectedErrorMsg1)) {
	            System.out.println("Internal server error message : " + internalErrorMsg);
	            // Explicit assertion for error condition for english internal server error
	            assert false : "Internal server error encountered";
	        }else {
	        	 System.out.println("No error response shown.");
	        }
	    } catch (Exception e) {
	        // Re-throw exception to propagate it up to the test framework
	        throw new AssertionError("An error occurred while checking for internal server error: " + e.getMessage(), e);
	    }
    }
    
//  // Method to check for internal server error
//  public static void checkForInternalServerError(WebDriver driver) {
//  	
//  	   try {
//  	        // Get the current URL
//  	        String currentUrl = driver.getCurrentUrl();
//
//  	        // Print the current URL
//  	        System.out.println("Current URL: " + currentUrl);
//  	        // Create a URI object from the current URL string
//  	        URI uri = new URI(currentUrl);
//  	        
//  	        // Open a connection to the URL
//  	        HttpURLConnection connection = (HttpURLConnection) uri.toURL().openConnection();
//  	        connection.setRequestMethod("POST");
//
//  	        int responseCode = connection.getResponseCode();
//  	        System.out.println("Request Response : " + responseCode);
//  	        Thread.sleep(5000);
//  	        if (responseCode == 500) {
//  	            System.err.println("Internal server error found for URL: " + uri);
//  	            // Explicit assertion for error condition
//  	            assert false : "Internal server error encountered";
//  	        }
//
//  	        connection.disconnect();
//  	    } catch (Exception e) {
//  	        // Re-throw exception to propagate it up to the test framework
//  	        throw new AssertionError("An error occurred while checking for internal server error: " + e.getMessage(), e);
//  	    }
//  }
}
