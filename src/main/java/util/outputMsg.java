package util;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class outputMsg {

	 // Method for accessing login output message
	 public static void loginMsg(WebDriver driver, String Message) {
		 // Find the div element with class attribute "warnmessage"
         WebElement warnMessageDiv = driver.findElement(By.className("warnmessage"));
         // Get the text content of the div element
         String divTextContent = warnMessageDiv.getText();
         assert divTextContent.trim().equals(Message);
	    }
	 
	 // Method for validating dashboard page
	 public static void dashboardMsg(WebDriver driver, String Message) {
	 	// Find the <body> element
        WebElement bodyElement = driver.findElement(By.tagName("body"));
        // get the text on body tag name
        String bodyText = bodyElement.getText();
        // Extract the required text "ログイン完了"
        String requiredText = bodyText.split("\n")[0];
        assert requiredText.trim().equals(Message);
        WebElement link = driver.findElement(By.linkText("戻る"));
        // Click the link
        link.click();
	 	}
	}

