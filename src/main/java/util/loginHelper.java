package util;

import java.time.Duration;


import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


public class loginHelper {

	 // Method for navigating login fields
	 public static void loginAction(WebDriver driver, String username, String password) {
		 // Set up explicit wait
		WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(5));
		
        WebElement usernameField = wait.until(ExpectedConditions.visibilityOfElementLocated(By.name("uid")));
        assert usernameField.isEnabled();
        usernameField.clear();
        usernameField.sendKeys(username);
        WebElement passwordField = wait.until(ExpectedConditions.visibilityOfElementLocated(By.name("password")));
        passwordField.clear();
        passwordField.sendKeys(password);
        WebElement loginBtn = wait.until(ExpectedConditions.visibilityOfElementLocated(By.name("loginSubmit")));
        loginBtn.click();
	    }
	}