package util;

import java.time.Duration;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.edge.EdgeDriver;

import io.github.bonigarcia.wdm.WebDriverManager;

public class browserSetup {
	
	// Method for accessing browser including browser setups
	public static WebDriver browserSettings() {
	    // Set up EdgeDriver using WebDriverManager
	    WebDriverManager.edgedriver().setup();
	    // Initialize EdgeDriver
	    WebDriver driver = new EdgeDriver();
	    // Set up implicit wait
	    driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(5));
	    // Navigate to the login page
	    driver.get("http://54.238.41.198:8080/exam/login");
	    driver.manage().window().maximize();
	    return driver;
	 }
}
