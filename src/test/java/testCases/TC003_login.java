package testCases;
import org.testng.annotations.Test;

import setups.annotationSetup;
import util.loginHelper;
import util.outputMsg;

public class TC003_login extends annotationSetup {
    
    @Test(priority = 0, enabled = true)
    public void performlogout() {
        try {
            // Empty user ID, password
            loginHelper.loginAction(driver, "sakamoto", "1234passWord");
            //validate login message 
            outputMsg.dashboardMsg(driver, "ログイン完了");
            // Call TC001_login for validation of login page
            TC001_login obj = new TC001_login();
            obj.verifyLoginLabels();
        } catch (Exception e) {
            // Print error message
            System.err.println("An error occurred: " + e.getMessage());
        }
    }
    

}

