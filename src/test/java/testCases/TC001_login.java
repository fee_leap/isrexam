package testCases;
import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;
import setups.annotationSetup;
import util.apiResponse;

public class TC001_login extends annotationSetup {


    @Test(priority = 0)
    public void verifyLoginLabels() {
    	
        try {
        	// Set up implicit wait
    	    driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
        	// Get the actual page title
            String actualTitle = driver.getTitle();
            // Define the expected page title
            String expectedTitle = "hogehoge";
            // Assert that the actual title matches the expected title
            Assert.assertEquals(actualTitle, expectedTitle, "Page title does not match the expected title");

            // Find the image element
            WebElement loginImage = driver.findElement(By.tagName("img"));
            String altAttributeValue = loginImage.getAttribute("alt");
            System.out.println("Login image: " + altAttributeValue);
            // Check if the login page is visible
            Assert.assertTrue(altAttributeValue.contains("login page"));
            
            // Check if the Login label visibility
            WebElement userIDLbl = driver.findElement(By.tagName("h2"));
            String loginLbl = userIDLbl.getText();
            Assert.assertTrue(loginLbl.contains("Login"));
            
            // Check if the User ID label visibility
            WebElement userIDLblElement = driver.findElement(By.cssSelector("label[for='uid']"));
            String userIDLblText = userIDLblElement.getText();
            Assert.assertTrue(userIDLblText.contains("ユーザーID"));
            
            // Check if the password label visibility
            WebElement pwdLblElement = driver.findElement(By.cssSelector("label[for='password']"));
            String pwdLblText = pwdLblElement.getText();
            Assert.assertTrue(pwdLblText.contains("パスワード"));
       
        } catch (AssertionError e) {
            // Print error message
            System.err.println("An error occurred: " + e.getMessage());
            throw e;
        }
        // Check for internal server error
        apiResponse.check500Error(driver);
    }
}

