package testCases;
import java.time.Duration;

import org.testng.annotations.Test;

import setups.annotationSetup;
import util.apiResponse;
import util.loginHelper;
import util.outputMsg;


public class TC002_login extends annotationSetup {

    @Test(priority = 0, enabled = true)
    public void performEmptyLogin() {
        try {
            // Empty user ID, password
            loginHelper.loginAction(driver, "", "");
            
            //validate login message 
            outputMsg.loginMsg(driver, "パスワードが未入力ﾃﾞｽ");
        } catch (Exception e) {
            // Print error message
            System.err.println("An error occurred: " + e.getMessage());
        }
        // Check for internal server error
        apiResponse.check500Error(driver);
    }

    @Test(priority = 1, enabled = true)
    public void performEmptyUserIDLogin() {
        try {
            // Empty user ID, random password
            loginHelper.loginAction(driver, "", "mypass");
            //validate login message 
            outputMsg.loginMsg(driver, "空のユーザーID");
        } catch (Exception e) {
            // Print error message
            System.err.println("An error occurred: " + e.getMessage());
        }
        // Check for internal server error
        apiResponse.check500Error(driver);
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
        driver.get("http://54.238.41.198:8080/exam/login");
    }
    
    @Test(priority = 2, enabled = true)
    public void performEmptyPasswordLogin() {
        try {
            // Valid user ID, empty password
            loginHelper.loginAction(driver, "sakamoto", "");
            //validate login message 
            outputMsg.loginMsg(driver, "パスワードが未入力ﾃﾞｽ");
        } catch (Exception e) {
            // Print error message
            System.err.println("An error occurred: " + e.getMessage());
        }
        // Check for internal server error
        apiResponse.check500Error(driver);
    }
    
    @Test(priority = 3, enabled = true)
    public void performNonExistUserID() {
        try {
            // Random user ID, random password
            loginHelper.loginAction(driver, "marco", "P@55w0rd");
            //validate login message 
            outputMsg.loginMsg(driver, "ログインＩＤが存在しません");
        } catch (Exception e) {
            // Print error message
            System.err.println("An error occurred: " + e.getMessage());
        }
        // Check for internal server error
        apiResponse.check500Error(driver);
    }
    
    @Test(priority = 4, enabled = true)
    public void performInvalidPassword() {
        try {
            // Valid user ID, random password
            loginHelper.loginAction(driver, "sakamoto", "P@55w0rd");
            //validate login message 
            outputMsg.loginMsg(driver, "パスワードが一致しません");
        } catch (Exception e) {
            // Print error message
            System.err.println("An error occurred: " + e.getMessage());
        }
        // Check for internal server error
        apiResponse.check500Error(driver);
    }
    
    @Test(priority = 5, enabled = true)
    public void performValidCredential() {
        try {
            // Valid user ID, valid password
            loginHelper.loginAction(driver, "sakamoto", "1234passWord");
            //validate login message 
            outputMsg.dashboardMsg(driver, "ログイン完了");
        } catch (Exception e) {
            // Print error message
            System.err.println("An error occurred: " + e.getMessage());
        }
    }
    
    @Test(priority = 6, enabled = false)
    public void performSimilarityUserID() {
        try {
            // Random user ID, random password
            loginHelper.loginAction(driver, "sakamoto15", "1234passWord");
            
            //validate login message 
            outputMsg.loginMsg(driver, "ログインＩＤが存在しません");
        } catch (Exception e) {
            // Print error message
            System.err.println("An error occurred: " + e.getMessage());
        }
        // Check for internal server error
        apiResponse.check500Error(driver);
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
        driver.get("http://54.238.41.198:8080/exam/login");
    }
}

