package insta;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import java.io.FileOutputStream;
import java.io.IOException;


public class instaFollowers {


	
	protected static WebDriver driver;
	@Test(priority = 0, enabled = true)
	
    public static void exportFollowers() {
            String url = "https://www.instagram.com/sunstarcebu/followers"; // URL of the webpage to scrape
            String cssQuery = "body > div.x1n2onr6.xzkaem6 > div.x9f619.x1n2onr6.x1ja2u2z > div > div.x1uvtmcs.x4k7w5x.x1h91t0o.x1beo9mf.xaigb6o.x12ejxvf.x3igimt.xarpa2k.xedcshv.x1lytzrv.x1t2pt76.x7ja8zs.x1n2onr6.x1qrby5j.x1jfb8zj > div > div > div > div > div.x7r02ix.xf1ldfh.x131esax.xdajt7p.xxfnqb6.xb88tzc.xw2csxc.x1odjw0f.x5fp0pe > div > div > div._aano > div:nth-child(1) > div > div:nth-child(1)"; // CSS class selector for the elements to scrape
            String excelFilePath = "output.xlsx"; // Output Excel file path

            try {
                // Connect to the webpage and get the document
                Document document = Jsoup.connect(url).get();

                // Get all elements with the specified class
                Elements elements = document.select(cssQuery);

                // Create a new Excel workbook
                try (Workbook workbook = new XSSFWorkbook()) {
                    Sheet sheet = workbook.createSheet("Data");

                    // Iterate through the elements and store their text content in the Excel file
                    int rowNum = 0;
                    for (Element element : elements) {
                        Row row = sheet.createRow(rowNum++);
                        row.createCell(0).setCellValue(element.text());
                    }

                    // Write the workbook to the output file
                    try (FileOutputStream outputStream = new FileOutputStream(excelFilePath)) {
                        workbook.write(outputStream);
                    }
                }

                System.out.println("Excel file created successfully.");
            } catch (IOException e) {
                e.printStackTrace();
            }
    }
}
