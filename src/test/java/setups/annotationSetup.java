package setups;

import java.time.Duration;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

import util.browserSetup;

public class annotationSetup {
	protected static WebDriver driver;

    @BeforeClass
    public static void setUp() {
        driver = browserSetup.browserSettings();
        // Set up implicit wait
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(5));
    }

    @AfterClass
    public static void tearDown() {
        if (driver != null) {
            driver.quit();
        }
    }
}